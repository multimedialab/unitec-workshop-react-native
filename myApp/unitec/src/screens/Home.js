import React from 'react';
import { Text, View, TouchableOpacity, Alert, StyleSheet, FlatList } from 'react-native';
import IconAnt from 'react-native-vector-icons/AntDesign';

const DATA = [
	{
	  id: '1',
	  title: 'First Item',
	},
	{
	  id: '2',
	  title: 'Second Item',
	},
	{
	  id: '3',
	  title: 'Third Item',
	},
	{
		id: '4',
		title: 'Four Item',
	},
];

const Item = ({ title }) => (
	<View style={estilos.item}>
	  <Text style={estilos.title}>{title}</Text>
	  <IconAnt name="staro" color="yellow" size={20} />
	</View>
  );

const Home = () => {

	const renderItem = ({ item }) => (
		<Item title={item.title} />
	);

	return(
		<View style={estilos.container}>
			<FlatList
				data={DATA}
				renderItem={renderItem}
				keyExtractor={item => item.id}
			/>
		</View>
	)
}

const estilos = StyleSheet.create({
	container: {
		flex: 1,
	},
	  item: {
		backgroundColor: '#b8b8b8',
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	},
	  title: {
		fontSize: 32,
	},
});

export default Home;