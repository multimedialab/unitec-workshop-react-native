import React from 'react';
import { View, Text, Alert, TouchableOpacity, StyleSheet, Image, TextInput } from 'react-native';

const SignUp = () => {
	return(
		<View style={estilos.container}>
			<Text style={estilos.title}>Registrate!</Text>
			<Text style={[estilos.textInit, {marginBottom: 30}]}>A continuación, dilegia todos los datos marcados con (*)</Text>

			<Text style={estilos.label}>Nombres:</Text>
			<TextInput
				style={estilos.input}
				placeholder="Nombres"
			/>

			<Text style={estilos.label}>Apellidos:</Text>
			<TextInput
				style={estilos.input}
				placeholder="Apellidos"
			/>

			<Text style={estilos.label}>Correo:</Text>
			<TextInput
				style={estilos.input}
				placeholder="Correo"
			/>

			<Text style={estilos.label}>Contraseña:</Text>
			<TextInput
				style={estilos.input}
				placeholder="Contraseña"
				secureTextEntry={true}
			/>

			<TouchableOpacity
				onPress={() => Alert.alert("Felicitaciones", "Te has registrado correctamente!")}
				style={estilos.customButton}
			>
				<Text style={{color: 'white', fontSize: 20}}>Registrarse</Text>
			</TouchableOpacity>

		</View>
	)
}

const estilos = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 5,
		paddingBottom: 5,
		paddingHorizontal: 20,
		alignItems: 'center'
	},
	title: {
		fontSize: 30,
		alignSelf: 'flex-start',
		color: 'blue',
		marginTop: 20,
		marginBottom: 25
	},
	textInit: {
		textAlign: 'center',
		paddingHorizontal: 30,
		fontWeight: "bold"
	},
	customButton: {
		width: "100%",
		height: 50,
		backgroundColor: 'blue',
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 50,
		marginTop: 20
	},
	contentImagen: {
		marginTop: 20,
		width: 250,
				height: 100,
				marginBottom: 40
	},
	imagen: {
		width: 250,
		height: 100,
		resizeMode: 'contain'
	},
	row: {
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center'
	},
	col6: {
		width: '50%',
		flexDirection: 'column',
		height: 100
	},
	input: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		width: '100%',
		borderRadius: 50,
		paddingLeft: 20,
		marginVertical: 10
	},
	label: {
		alignSelf: 'flex-start',
		fontWeight: 'bold',
		fontSize: 16
	}
});

export default SignUp;