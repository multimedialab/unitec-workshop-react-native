import React, { useState } from 'react';
import { View, Text, Alert, TouchableOpacity, StyleSheet, Image, TextInput } from 'react-native';

const Login = ({ navigation }) => {

	const [value, onChangeText] = useState('');
	const [password, onChangePassword] = useState('');

	return(
		<View style={estilos.container}>
			<View style={estilos.contentImagen}>
				<Image style={estilos.imagen} source={require("../assets/images/logo.png")} />
			</View>

			<Text style={estilos.label}>Correo:</Text>
			<TextInput
				style={estilos.input}
				placeholder="Correo"
				onChangeText={text => onChangeText(text)}
				value={value}
			/>

			<Text style={estilos.label}>Contraseña:</Text>
			<TextInput
				style={estilos.input}
				placeholder="Contraseña"
				secureTextEntry={true}
				onChangeText={text => onChangePassword(text)}
				value={password}
			/>

			<TouchableOpacity
				onPress={() => navigation.navigate('Home')}
				style={estilos.customButton}
			>
				<Text style={{color: 'white', fontSize: 20}}>Ingresar</Text>
			</TouchableOpacity>

			<TouchableOpacity
				onPress={() => navigation.navigate('SignUp')}
				style={[estilos.customButton, {backgroundColor: 'gray'}]}
			>
				<Text style={{color: 'white', fontSize: 20}}>Crear Cuenta</Text>
			</TouchableOpacity>
		</View>
	)
}

const estilos = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  customButton: {
    width: "100%",
    height: 50,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    marginTop: 20
  },
  contentImagen: {
    marginTop: 20,
    width: 250,
		height: 100,
		marginBottom: 40
  },
  imagen: {
    width: 250,
    height: 100,
    resizeMode: 'contain'
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  col6: {
    width: '50%',
    flexDirection: 'column',
    height: 100
	},
	input: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		width: '100%',
		borderRadius: 50,
		paddingLeft: 20,
		marginVertical: 10
	},
	label: {
		alignSelf: 'flex-start',
		fontWeight: 'bold',
		fontSize: 16
	}
});

export default Login;