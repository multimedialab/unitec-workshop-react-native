import React from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';

const MyAccount = () => {
	return(
		<View style={estilos.container}>
			<ImageBackground source={require("../assets/images/bgCuenta.jpg")} style={estilos.image}>
				<Text style={estilos.text}>Inside</Text>
			</ImageBackground>
  </View>
	)
}


const estilos = StyleSheet.create({
	container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0"
  }
});

export default MyAccount;