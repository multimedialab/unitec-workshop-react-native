import React from 'react';
// Navegacion con Stack
import { createStackNavigator } from '@react-navigation/stack';

// Llamar TabNavigation
import TabNavigation from './TabNavigation';

// Llamar Screen
import Login from '../screens/Login';
import SignUp from '../screens/SignUp';
import Home from '../screens/Home';

const Stack = createStackNavigator();

const Navigation = () => {
	return(
		<Stack.Navigator>
			<Stack.Screen
				name="Login"
				component={Login}
				options={{
					title: "Bienvenido",
					headerShown: false
				}}
			/>
			<Stack.Screen
				name="SignUp"
				component={SignUp}
				options={{
					title: '',
					headerStyle: {
						backgroundColor: 'transparent',
						elevation: 0
					}
				}}
			/>
			<Stack.Screen
				name="Home"
				component={TabNavigation}
				options={{
					headerShown: false
				}}
			/>
		</Stack.Navigator>
	)
}

export default Navigation;