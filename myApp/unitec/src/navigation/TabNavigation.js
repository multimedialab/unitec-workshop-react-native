import React from 'react';
// Navegacion con Tabs
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//Iconos
import Ionicons from 'react-native-vector-icons/Ionicons';

// Llamar Screens
import Home from '../screens/Home';
import MyAccount from '../screens/MyAccount';
import Login from '../screens/Login';

const Tab = createBottomTabNavigator();

const TabNavigation = () => {
	return(
		<Tab.Navigator
			screenOptions={({ route }) => ({
				tabBarIcon: ({ focused, color, size }) => {
					let iconName;
					if (route.name === 'Home') {
						iconName = focused
							? 'home'
							: 'home-outline';
					} else if (route.name === 'MiCuenta') {
						iconName = focused ? 'settings' : 'settings-outline';
					}
					return <Ionicons name={iconName} size={size} color={color} />;
				},
			})}
			tabBarOptions={{
				activeTintColor: 'blue',
				inactiveTintColor: 'gray',
			}}
		>
			<Tab.Screen name="Home" component={Home} />
			<Tab.Screen name="MiCuenta" component={MyAccount} />
		</Tab.Navigator>
	)
}

export default TabNavigation;