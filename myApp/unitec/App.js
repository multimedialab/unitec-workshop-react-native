import React from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity } from 'react-native';
import 'react-native-gesture-handler';

//Navegacion
import { NavigationContainer } from '@react-navigation/native';
import Navigation from './src/navigation/Navigation'; //Stack
import TabNavigation from './src/navigation/TabNavigation'; //Tab

const App = () => {
  return (
    <NavigationContainer>
      <Navigation />
    </NavigationContainer>
  );
};

export default App;
